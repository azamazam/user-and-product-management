﻿using PMS.Entities;
using PMS.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPrac.Security;

namespace WebPrac.Controllers {
	public class UserController:Controller {
		[HttpGet]
		public ActionResult Login() {
			return View();
		}

		[HttpPost]
		public ActionResult Login(String login, String password) {
			var obj = PMS.BAL.UserBO.ValidateUser(login, password);
			if (obj != null) {
				Session["user"] = obj;
				if (obj.IsAdmin == true)
					return Redirect("~/Home/Admin");
				else
					return Redirect("~/Home/NormalUser");
			}

			ViewBag.MSG = "Invalid Login/Password";
			ViewBag.Login = login;

			return View();
		}

		[HttpGet]

		public ActionResult Register() {
			return View();
		}

		[HttpPost]
		public ActionResult Save(UserDTO dto) {
			//User Save Logic


			var uniqueName = "";
			if (Request.Files["PictureName"] != null) {
				var file = Request.Files["PictureName"];
				if (file.FileName != "") {
					var ext = System.IO.Path.GetExtension(file.FileName);

					//Generate a unique name using Guid
					uniqueName = Guid.NewGuid().ToString() + ext;

					//Get physical path of our folder where we want to save images
					var rootPath = Server.MapPath("~/UploadedFiles");

					var fileSavePath = System.IO.Path.Combine(rootPath, uniqueName);

					// Save the uploaded file to "UploadedFiles" folder
					file.SaveAs(fileSavePath);

					dto.PictureName = uniqueName;
				}
			}


			PMS.BAL.UserBO.Save(dto);

			TempData["Msg"] = "Record is saved!";

			UserDTO obj = ( UserDTO )Session["user"];
			if (obj != null) {
				Session["user"] = dto;
				if (obj.IsAdmin == true)
					return Redirect("~/Home/Admin");
				else
					return Redirect("~/Home/NormalUser");
			}
			return RedirectToAction("login");
		}

		[HttpGet]
		public ActionResult Logout() {
			SessionManager.ClearSession();
			return RedirectToAction("Login");
		}


		[HttpGet]
		public ActionResult Login2() {
			return View();
		}

		[HttpPost]
		public JsonResult ValidateUser(String login, String password) {

			Object data = null;

			try {
				var url = "";
				var flag = false;

				var obj = PMS.BAL.UserBO.ValidateUser(login, password);
				if (obj != null) {
					flag = true;
					SessionManager.User = obj;

					if (obj.IsAdmin == true)
						url = Url.Content("~/Home/Admin");
					else
						url = Url.Content("~/Home/NormalUser");
				}

				data = new {
					valid = flag,
					urlToRedirect = url
				};
			}
			catch (Exception) {
				data = new {
					valid = false,
					urlToRedirect = ""
				};
			}

			return Json(data, JsonRequestBehavior.AllowGet);
		}
		[HttpGet]
		public ActionResult editUser() {
			var obj = ( UserDTO )Session["user"];
			if (obj == null)
				return RedirectToAction("login");

			var userdto = Session["user"];
			return View(userdto);
		}

		[HttpGet]
		public ActionResult changePassword() {
			var obj = ( UserDTO )Session["user"];
			if (obj == null)
				return RedirectToAction("login");

			return View();
		}
		[HttpPost]
		public ActionResult changePassword(string password, string newpassword) {
			var obj = ( UserDTO )Session["user"];
			if (obj == null)
				return RedirectToAction("login");

			if ( password != obj.Password ) {
				ViewBag.MSG = "Old password is  incorrect";
				return View();
			}

			obj.Password = newpassword;
			Session["user"] = obj;
			PMS.BAL.UserBO.UpdatePassword(obj);
		
			if (obj.IsAdmin == true)
				return Redirect("~/Home/Admin");
			else
				return Redirect("~/Home/NormalUser");

		}

		[HttpPost]
		public ActionResult resetPassword( string newpassword) {
			var obj = ( UserDTO )Session["u"];
			if (obj == null)
				return RedirectToAction("login");


			obj.Password = newpassword;
			
			Session.Remove("u");
			PMS.BAL.UserBO.UpdatePassword(obj);
			ViewBag.MSG = "password chnaged";
			return RedirectToAction("login");
		}
		[HttpGet]
		public ActionResult forgotpassword() {
			return View();
		}
		[HttpPost]
		public ActionResult EnterCode(string login) {
			//emailsend 
			UserDTO dto= PMS.BAL.UserBO.GetUserByLogin(login);
			Session["u"] = dto;   
			Random r = new Random();
			string code= r.Next(1000, 10000).ToString();

			EmailHandler emailhandler = new EmailHandler();
			string subject ="verification Code";
			string body =string.Format( "<span  style=\'font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif;font-size:16px;line-height:21px;color:#141823/'>" +
			"<p>Hi {0},</p><p>We received a request to reset your password.</p>" +
			"You can enter the following password reset code:<table border=\'0\' ><tbody><tr>" +
			"<td style=\"font-size:11px;font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;padding:10px;background-color:#f2f2f2;border-left:1px solid #ccc;border-right:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc\">" +
			"<span  style=\"font-family:Helvetica Neue,Helvetica,Lucida Grande,tahoma,verdana,arial,sans-serif;font-size:16px;line-height:21px;color:#141823\">" +
			"{1}</span></td></tr></tbody></table><div><span style=\"color:#333333;font-weight:bold\">Didn't request this change?</span></div>If you didn't request a new password, let us know.</span>",dto.Name,code);
			
			emailhandler.sendEmail(dto.email,subject,body);
			Session["code"] = code;
			return View(dto);
		}

		[HttpPost]
		public ActionResult verifyCode(string code) {
			if (code.Equals(Session["code"])) {
				Session.Remove("code");
				return RedirectToAction("newPassword");
			}
			ViewBag.MSG = "Entered Code is not same which is sent ";
			return View("EnterCode", Session["u"]);
		}

		public ActionResult newPassword(string code) {

			return View();
		}

		[HttpGet]
		public ActionResult showProfile(int id=-1) {
			var obj = ( UserDTO )Session["user"];
			if (obj == null)
				return RedirectToAction("login");
			else if (id==-1) {
				id = obj.UserID;
			}

			UserDTO userdto = PMS.BAL.UserBO.GetUserById(id);
			return View(userdto);
		}
		
	}
}
